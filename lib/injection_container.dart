import 'package:get_it/get_it.dart';
import 'package:tap_design/dependices/app_http_client.dart';
import 'package:tap_design/screens/data/data_source/remote_data_source.dart';
import 'package:tap_design/screens/data/mappers/interest_response_to_model.dart';
import 'package:tap_design/screens/data/mappers/profile_reponse_to_model.dart';
import 'package:tap_design/screens/data/repository/user_repo_impl.dart';
import 'package:tap_design/screens/domain/repositories/user_repo.dart';
import 'package:tap_design/screens/domain/usecases/get_interest_use_case.dart';
import 'package:tap_design/screens/domain/usecases/get_user_profile_use_case.dart';

import 'config/api_config.dart';

final sl = GetIt.instance;

Future<void> init() async {
//tegister api stuff
  sl.registerLazySingleton(() => ApiConfig());
  sl.registerLazySingleton<ApiRequest>(() => ApiRequestImpl(sl()));
// registers mappers
  sl.registerLazySingleton(() => InterestResponseToModel());
  sl.registerLazySingleton(() => ProfileResponseModelToProfileEntity());
//  register data source
  sl.registerLazySingleton<ApiRemoteDataSource>(
      () => ApiRemoteDataSourceImpl(sl()));
// register repo
  sl.registerLazySingleton<UserRepo>(() => UserRepoImpl(sl(), sl(), sl()));
//  register use case
  sl.registerLazySingleton(() => GetInterestUseCase(sl()));
  sl.registerLazySingleton(() => GetUserProfileUseCase(sl()));
}
