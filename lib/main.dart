import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:get/get.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:tap_design/screens/app_state/app_state.dart';
import 'package:tap_design/screens/app_state/reducer.dart';

import 'package:tap_design/screens/passion_scrren/choose_passions_screen.dart';
import 'package:tap_design/screens/passion_scrren/state/passion_store.dart';
import 'package:tap_design/screens/passion_scrren/state/passoins_reducer.dart';
import 'package:tap_design/screens/profile_screen/state/profile_store.dart';

import 'injection_container.dart' as di;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final Store<AppState> store = Store<AppState>(
    appStateReducer,
    initialState: AppState(
        passionSate: PassionSate(selectedPassoins: [], passions: []),
        profileState: ProfileState(selectedSlider: 0)),
    middleware: [thunkMiddleware],
  );

  // MyApp(this._store);

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: this.store,
      child: GetMaterialApp(
          title: 'Flutter Demo',
          debugShowCheckedModeBanner: false,
          home: ChoosePassionScreen()),
    );
  }
}
