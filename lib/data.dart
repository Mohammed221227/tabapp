//
//
//  List<Map> list = [
//   {
//     "flag": false,
//     "_id": "605983cceb72561fb6bb0679",
//     "name": "Rock climbing",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:59:40.871Z",
//     "slug": "rock-climbing",
//     "__v": 0,
//     "image": "interests-1625554051716.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "605983c2eb72561fb6bb0678",
//     "name": "Yoga",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:59:30.318Z",
//     "slug": "yoga",
//     "__v": 0,
//     "image": "interests-1625554085969.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598385eb72561fb6bb0677",
//     "name": "Interior decoration",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:58:29.750Z",
//     "slug": "interior-decoration",
//     "__v": 0,
//     "image": "interests-1625554131252.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598379eb72561fb6bb0676",
//     "name": "Comic books",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:58:17.154Z",
//     "slug": "comic-books",
//     "__v": 0,
//     "image": "interests-1625554160691.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "6059836deb72561fb6bb0675",
//     "name": "Learning instruments",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:58:05.417Z",
//     "slug": "learning-instruments",
//     "__v": 0,
//     "image": "interests-1625554209547.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598362eb72561fb6bb0674",
//     "name": "Sewing",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:57:54.606Z",
//     "slug": "sewing",
//     "__v": 0,
//     "image": "interests-1625554235276.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598357eb72561fb6bb0673",
//     "name": "Stand-up comedy",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:57:43.125Z",
//     "slug": "stand-up-comedy",
//     "__v": 0,
//     "image": "interests-1625554281191.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598349eb72561fb6bb0672",
//     "name": "Writing",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:57:29.003Z",
//     "slug": "writing",
//     "__v": 0,
//     "image": "interests-1625554320555.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598338eb72561fb6bb0671",
//     "name": "Learning languages",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:57:12.568Z",
//     "slug": "learning-languages",
//     "__v": 0,
//     "image": "interests-1625554361736.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "6059832ceb72561fb6bb0670",
//     "name": "Puzzles",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:57:00.993Z",
//     "slug": "puzzles",
//     "__v": 0,
//     "image": "interests-1625554397200.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "6059831eeb72561fb6bb066f",
//     "name": "Baking",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:56:46.539Z",
//     "slug": "baking",
//     "__v": 0,
//     "image": "interests-1625554430703.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598300eb72561fb6bb066e",
//     "name": "Volunteering",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:56:16.925Z",
//     "slug": "volunteering",
//     "__v": 0,
//     "image": "interests-1625554463605.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "605982f7eb72561fb6bb066d",
//     "name": "Craft-making",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:56:07.047Z",
//     "slug": "craft-making",
//     "__v": 0,
//     "image": "interests-1625554497048.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "605982eaeb72561fb6bb066c",
//     "name": "Fitness and exercise",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:55:54.413Z",
//     "slug": "fitness-and-exercise",
//     "__v": 0,
//     "image": "interests-1625554521789.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "605982dceb72561fb6bb066b",
//     "name": "Reading",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:55:40.479Z",
//     "slug": "reading",
//     "__v": 0,
//     "image": "interests-1625554547168.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "605982cfeb72561fb6bb066a",
//     "name": "Dance",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:55:27.995Z",
//     "slug": "dance",
//     "__v": 0,
//     "image": "interests-1625554573839.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "605982c5eb72561fb6bb0669",
//     "name": "Photography",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:55:17.295Z",
//     "slug": "photography",
//     "__v": 0,
//     "image": "interests-1625554605399.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "605982baeb72561fb6bb0668",
//     "name": "Watching TV",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:55:06.101Z",
//     "slug": "watching-tv",
//     "__v": 0,
//     "image": "interests-1625554630315.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "605982aeeb72561fb6bb0667",
//     "name": "Gambling",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:54:54.305Z",
//     "slug": "gambling",
//     "__v": 0,
//     "image": "interests-1625554650063.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "605982a2eb72561fb6bb0666",
//     "name": "Partying",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:54:42.811Z",
//     "slug": "partying",
//     "__v": 0,
//     "image": "interests-1625554684565.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598295eb72561fb6bb0665",
//     "name": "Social media",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:54:29.075Z",
//     "slug": "social-media",
//     "__v": 0,
//     "image": "interests-1625554712614.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598284eb72561fb6bb0664",
//     "name": "Music",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:54:12.365Z",
//     "slug": "music",
//     "__v": 0,
//     "image": "interests-1625554734362.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598279eb72561fb6bb0663",
//     "name": "Cycling",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:54:01.976Z",
//     "slug": "cycling",
//     "__v": 0,
//     "image": "interests-1625554762445.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "6059826deb72561fb6bb0662",
//     "name": "Philanthropy",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:53:49.086Z",
//     "slug": "philanthropy",
//     "__v": 0,
//     "image": "interests-1625554800581.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "6059825feb72561fb6bb0661",
//     "name": "Pet care",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:53:35.247Z",
//     "slug": "pet-care",
//     "__v": 0,
//     "image": "interests-1625554829197.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598253eb72561fb6bb0660",
//     "name": "Singing",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:53:23.031Z",
//     "slug": "singing",
//     "__v": 0,
//     "image": "interests-1625554864634.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598243eb72561fb6bb065f",
//     "name": "Cooking",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:53:07.772Z",
//     "slug": "cooking",
//     "__v": 0,
//     "image": "interests-1625554888131.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598239eb72561fb6bb065e",
//     "name": "Watching sports",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:52:57.090Z",
//     "slug": "watching-sports",
//     "__v": 0,
//     "image": "interests-1625554924832.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598229eb72561fb6bb065d",
//     "name": "Video games ",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:52:41.211Z",
//     "slug": "video-games",
//     "__v": 0,
//     "image": "interests-1625554949670.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598218eb72561fb6bb065c",
//     "name": "Internet surfing ",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:52:24.912Z",
//     "slug": "internet-surfing",
//     "__v": 0,
//     "image": "interests-1625554983729.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598209eb72561fb6bb065b",
//     "name": "Collections",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:52:09.220Z",
//     "slug": "collections",
//     "__v": 0,
//     "image": "interests-1625555006287.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598200eb72561fb6bb065a",
//     "name": "Trekking and Camping",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:52:00.117Z",
//     "slug": "trekking-and-camping",
//     "__v": 0,
//     "image": "interests-1625555039934.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "6059817eeb72561fb6bb0659",
//     "name": "Traveling",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:49:50.416Z",
//     "slug": "traveling",
//     "__v": 0,
//     "image": "interests-1625555061592.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598174eb72561fb6bb0658",
//     "name": "Shopping",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:49:40.079Z",
//     "slug": "shopping",
//     "__v": 0,
//     "image": "interests-1625555090166.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "6059816beb72561fb6bb0657",
//     "name": "Swimming",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:49:31.388Z",
//     "slug": "swimming",
//     "__v": 0,
//     "image": "interests-1625555118476.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "6059815deb72561fb6bb0656",
//     "name": "Gardening",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:49:17.987Z",
//     "slug": "gardening",
//     "__v": 0,
//     "image": "interests-1625555137308.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598153eb72561fb6bb0655",
//     "name": "Drawing and painting",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:49:07.975Z",
//     "slug": "drawing-and-painting",
//     "__v": 0,
//     "image": "interests-1625555167097.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598149eb72561fb6bb0654",
//     "name": "Sports and games",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:48:57.869Z",
//     "slug": "sports-and-games",
//     "__v": 0,
//     "image": "interests-1625555211722.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "6059813aeb72561fb6bb0653",
//     "name": "Outdoor activities",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:48:42.283Z",
//     "slug": "outdoor-activities",
//     "__v": 0,
//     "image": "interests-1625555235582.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "6059812ceb72561fb6bb0652",
//     "name": "Walking",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:48:28.788Z",
//     "slug": "walking",
//     "__v": 0,
//     "image": "interests-1625555265865.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "6059811feb72561fb6bb0651",
//     "name": "Running",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:48:15.569Z",
//     "slug": "running",
//     "__v": 0,
//     "image": "interests-1625555287662.jpeg"
//   },
//   {
//     "flag": false,
//     "_id": "60598115eb72561fb6bb0650",
//     "name": "Movies and TV shows",
//     "category": "60597fa4eb72561fb6bb064f",
//     "date": "2021-03-23T05:48:05.914Z",
//     "slug": "movies-and-tv-shows",
//     "__v": 0,
//     "image": "interests-1625555323026.jpeg"
//   }
// ];