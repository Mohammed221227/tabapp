import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:tap_design/config/api_config.dart';

enum RequestType { GET, POST, PUT, DELETE }

class RequestException implements Exception {
  String message;

  RequestException({this.message = "Error While do operation"});

  @override
  String toString() {
    // TODO: implement toString
    return message;
  }
}

class StatusCode {
  static const int success = 200;
  static const int unAuthorized = 401;
//unAuthorized
}

class RequestOption {
  String url;
  String token;
  dynamic body;
  RequestType requestType;

  RequestOption({@required this.url, this.body, this.token, this.requestType});
}

abstract class ApiRequest {
  Future<http.Response> get({@required RequestOption requestOption});

  Future<http.Response> post({@required RequestOption requestOption});

  Future<http.Response> put({@required RequestOption requestOption});

  Future<http.Response> delete({@required RequestOption requestOption});

  Map encodeResponseDate({@required http.Response response});

  List<dynamic> encodeResponseListOfDate({@required http.Response response});
}

class ApiRequestImpl implements ApiRequest {
  ApiConfig _apiConfig;

  ApiRequestImpl(this._apiConfig);

  @override
  Future<http.Response> get({@required RequestOption requestOption}) async {
    print("Requesting ------------------> ${requestOption.url}");
    requestOption.requestType = RequestType.GET;
    return await sendRequestAndReturnData(requestOption: requestOption);
  }

  @override
  Future<http.Response> delete({@required RequestOption requestOption}) async {
    requestOption.requestType = RequestType.DELETE;

    return await sendRequestAndReturnData(requestOption: requestOption);
  }

  @override
  Future<http.Response> post({@required RequestOption requestOption}) async {
    requestOption.requestType = RequestType.POST;

    return await sendRequestAndReturnData(requestOption: requestOption);
  }

  @override
  Future<http.Response> put({@required RequestOption requestOption}) async {
    requestOption.requestType = RequestType.PUT;

    return await sendRequestAndReturnData(requestOption: requestOption);
  }

  Future<http.Response> sendRequestAndReturnData(
      {@required RequestOption requestOption}) async {
    print("send request -----------------------------------------------");

    try {
      return await executeRequest(requestOption: requestOption);
    } catch (e) {
      print("exception ----------------------------------> ${e.toString()}");
      throw RequestException(message: e.toString());
    }
  }

  Future<http.Response> executeRequest(
      {@required RequestOption requestOption}) async {
    print("exe request -----------------------------------------------");

    return processResponseDataData(
        response: await getRequestResponse(requestOption: requestOption));
  }

  Future<http.Response> getRequestResponse(
      {@required RequestOption requestOption}) async {
    switch (requestOption.requestType) {
      case RequestType.GET:
        return getResponse(requestOption: requestOption);
        break;
      case RequestType.POST:
        return getPostResponse(requestOption: requestOption);
        break;
      case RequestType.PUT:
        return getPutResponse(requestOption: requestOption);

        break;
      case RequestType.DELETE:
        return getDeleteResponse(requestOption: requestOption);

        break;
      default:
        return getResponse(requestOption: requestOption);
    }
  }

  Future<http.Response> getResponse(
      {@required RequestOption requestOption}) async {
    return await http.get("${_apiConfig.baseUrl}${requestOption.url}",
        headers: getRequestOptions(token: requestOption.token));
  }

  Future<http.Response> getPostResponse(
      {@required RequestOption requestOption}) async {
    String url = "${_apiConfig.baseUrl}${requestOption.url}";
    print("SEND REQUEST TO  ======================>  $url");
    var requestHeader = getRequestOptions(token: requestOption.token);
    return requestOption.body != null
        ? await http.post(url,
            headers: requestHeader, body: json.encode(requestOption.body))
        : await http.post(url, headers: requestHeader);
  }

  Future<http.Response> getPutResponse(
      {@required RequestOption requestOption}) async {
    String url = "${_apiConfig.baseUrl}${requestOption.url}";
    var requestHeader = getRequestOptions(token: requestOption.token);
    return requestOption.body != null
        ? await http.put(url, headers: requestHeader, body: requestOption.body)
        : await http.put(url, headers: requestHeader);
  }

  Future<http.Response> getDeleteResponse(
      {@required RequestOption requestOption}) async {
    return await http.delete("${_apiConfig.baseUrl}${requestOption.url}",
        headers: getRequestOptions(token: requestOption.token));
  }

  processResponseDataData({@required @required http.Response response}) {
    if (response.statusCode == StatusCode.success) {
      print(
          "process request -----------------------------------------------   ${response.statusCode}");

      return response;
    } else if (response.statusCode == StatusCode.unAuthorized) {
      throw RequestException(
          message: "This endpoint required access token to get data");
    } else {
      throw RequestException(message: response.reasonPhrase);
    }
  }

  getRequestOptions({@required String token}) {
    return token != null
        ? {
            "Authorization": token,
            "X-ApiKey": _apiConfig.apiKey,
            "Accept": "application/json",
            "content-type": "application/json",
          }
        : {
            "X-ApiKey": _apiConfig.apiKey,
            "Accept": "application/json",
            "content-type": "application/json",
          };
  }

  @override
  Map encodeResponseDate({http.Response response}) {
    return jsonDecode(response.body);
  }

  @override
  List<dynamic> encodeResponseListOfDate({http.Response response}) {
    List<dynamic> list = json.decode(response.body);

    return list;
  }
}

typedef CreateModelFromJson = dynamic Function(Map<String, dynamic> json);
typedef CreateListOfModelsFromJson = dynamic Function(
    List<Map<String, dynamic>> json);

// we will create factory here
