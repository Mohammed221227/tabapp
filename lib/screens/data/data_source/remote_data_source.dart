import 'dart:convert';

import 'package:http/http.dart';
import 'package:tap_design/config/api_config.dart';
import 'package:tap_design/dependices/app_http_client.dart';
import 'package:tap_design/screens/data/model/interests_model.dart';
import 'package:tap_design/screens/data/model/profile_reponse_model.dart';

abstract class ApiRemoteDataSource {
  Future<InterestsResponseModel> getInterest();

  Future<ProfileResponseModel> getUserProfileData();
}

class ApiRemoteDataSourceImpl implements ApiRemoteDataSource {
  ApiRequest _apiRequest;

  ApiRemoteDataSourceImpl(this._apiRequest);

  @override
  Future<InterestsResponseModel> getInterest() async {
    try {
      Response response = await _apiRequest.get(
          requestOption: RequestOption(
        url: GET_INTEREST_FLAG,
      ));
      print(response);
      return InterestsResponseModel.fromJson(json.decode(response.body));
    } on RequestException catch (e) {
      print(e);
      throw RequestException(message: e.toString());
    }
  }

  @override
  Future<ProfileResponseModel> getUserProfileData() async {
    try {
      Response response = await _apiRequest.get(
          requestOption: RequestOption(url: GET_PROFILE_FLAG, token: token));
      print(response);
      return ProfileResponseModel.fromJson(json.decode(response.body));
    } on RequestException catch (e) {
      print(e);
      throw RequestException(message: e.toString());
    }
  }
}
