import 'package:tap_design/dependices/app_http_client.dart';
import 'package:tap_design/screens/data/data_source/remote_data_source.dart';
import 'package:tap_design/screens/data/mappers/interest_response_to_model.dart';
import 'package:tap_design/screens/data/mappers/profile_reponse_to_model.dart';
import 'package:tap_design/screens/data/model/interests_model.dart';
import 'package:tap_design/screens/data/model/profile_reponse_model.dart';
import 'package:tap_design/screens/domain/entities/interest_model.dart';
import 'package:tap_design/screens/domain/entities/user_profile_model.dart';
import 'package:tap_design/screens/domain/repositories/user_repo.dart';

class UserRepoImpl implements UserRepo {
  ApiRemoteDataSource _apiRemoteDataSource;
  InterestResponseToModel _interestResponseToModel;
  ProfileResponseModelToProfileEntity _profileResponseModelToProfileEntity;

  UserRepoImpl(this._apiRemoteDataSource, this._interestResponseToModel,
      this._profileResponseModelToProfileEntity);

  @override
  Future<List<InterestModel>> getInterests() async {
    try {
      InterestsResponseModel interestsResponseModel =
          await this._apiRemoteDataSource.getInterest();

      return _interestResponseToModel
          .map(interestsResponseModel.data.interests);
    } on RequestException catch (e) {
      return [];
    }
  }

  @override
  Future<UserProfileModel> getPersonProfileData() async {
    try {
      ProfileResponseModel profileResponseModel =
          await this._apiRemoteDataSource.getUserProfileData();

      return _profileResponseModelToProfileEntity.map(profileResponseModel);
    } on RequestException catch (e) {
      print(e.toString());
      return UserProfileModel(
          basicInfo: PersonBasicInfo(
              name: "",
              jobTitle: "",
              bio: "",
              profilePicture: "",
              age: 0,
              gender: ""),
          passions: [],
          personalInfo: []);
    }
  }
}
