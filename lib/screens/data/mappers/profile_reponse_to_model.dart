import 'package:tap_design/screens/data/model/profile_reponse_model.dart';
import 'package:tap_design/screens/domain/entities/interest_model.dart';
import 'package:tap_design/screens/domain/entities/user_profile_model.dart';
import 'package:tap_design/utils/type_mapper.dart';

class ProfileResponseModelToProfileEntity
    implements TypeMapper<ProfileResponseModel, UserProfileModel> {
  @override
  UserProfileModel map(ProfileResponseModel from) {
    // TODO: implement map
    return UserProfileModel(
        passions: from.data.profile.interests
            .map((passion) => InterestModel(
                image: passion.image, id: passion.sId, name: passion.name))
            .toList(),
        personalInfo: from.data.profile.basicInfo
            .map((info) =>
                PersonalInfo(question: info.key.name, answer: info.value))
            .toList(),
        basicInfo: PersonBasicInfo(
            age: from.data.profile.age,
            bio: from.data.profile.bio,
            name: from.data.profile.name,
            jobTitle: from.data.profile.jobTitle,
            gender: from.data.profile.gender,
            profilePicture: from.data.profile.profilePicture));
  }
}
