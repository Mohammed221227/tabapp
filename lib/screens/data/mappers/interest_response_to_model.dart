import 'package:tap_design/screens/data/model/interests_model.dart';
import 'package:tap_design/screens/domain/entities/interest_model.dart';
import 'package:tap_design/utils/type_mapper.dart';

class InterestResponseToModel
    implements TypeMapper<List<Interests>, List<InterestModel>> {
  @override
  List<InterestModel> map(List<Interests> from) {
    return from
        .map((interest) => InterestModel(
            id: interest.sId, name: interest.name, image: interest.image))
        .toList();
  }
}
