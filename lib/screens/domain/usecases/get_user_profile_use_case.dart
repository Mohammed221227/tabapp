import 'package:tap_design/screens/domain/entities/interest_model.dart';
import 'package:tap_design/screens/domain/entities/user_profile_model.dart';
import 'package:tap_design/screens/domain/repositories/user_repo.dart';
import 'package:tap_design/utils/usecase.dart';

class GetUserProfileUseCase extends UseCase<UserProfileModel, NoParams> {
  UserRepo _userRepo;

  GetUserProfileUseCase(this._userRepo);

  @override
  Future<UserProfileModel> call({NoParams params}) {
    return _userRepo.getPersonProfileData();
  }
}
