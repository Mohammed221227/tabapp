import 'package:tap_design/screens/domain/entities/interest_model.dart';
import 'package:tap_design/screens/domain/repositories/user_repo.dart';
import 'package:tap_design/utils/usecase.dart';

class GetInterestUseCase extends UseCase<List<InterestModel>, NoParams> {
  UserRepo _userRepo;

  GetInterestUseCase(this._userRepo);

  @override
  Future<List<InterestModel>> call({NoParams params}) {
    return _userRepo.getInterests();
  }
}
