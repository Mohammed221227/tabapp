


import 'package:tap_design/screens/domain/entities/interest_model.dart';
import 'package:tap_design/screens/domain/entities/user_profile_model.dart';

abstract class UserRepo {
  Future<List<InterestModel>> getInterests();

  Future<UserProfileModel> getPersonProfileData();

}