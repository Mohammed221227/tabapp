import 'package:flutter/cupertino.dart';

class InterestModel {
  String name;
  String image;
  String id;

  InterestModel({@required this.id, @required this.name, @required this.image});
}
