import 'package:flutter/cupertino.dart';
import 'package:tap_design/screens/domain/entities/interest_model.dart';

class UserProfileModel {
  PersonBasicInfo basicInfo;
  List<PersonalInfo> personalInfo;
  List<InterestModel> passions;

  UserProfileModel(
      {@required this.basicInfo,
      @required this.personalInfo,
      @required this.passions});
}

class PersonalInfo {
  String question;
  String answer;

  PersonalInfo({@required this.question, @required this.answer});

  PersonalInfo.fromJson(Map<String, dynamic> json) {
    question = json['question'];
    answer = json['answer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['question'] = this.question;
    data['answer'] = this.answer;
    return data;
  }
}

class PersonBasicInfo {
  String name;
  String profilePicture;
  String gender;
  int age;
  String bio;
  String jobTitle;

  PersonBasicInfo(
      {@required this.name,
      @required this.profilePicture,
      @required this.gender,
      @required this.age,
      @required this.bio,
      @required this.jobTitle});

  PersonBasicInfo.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    profilePicture = json['profile_picture'];
    gender = json['gender'];
    age = json['age'];
    bio = json['bio'];
    jobTitle = json['job_title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['profile_picture'] = this.profilePicture;
    data['gender'] = this.gender;
    data['age'] = this.age;
    data['bio'] = this.bio;
    data['job_title'] = this.jobTitle;
    return data;
  }
}
