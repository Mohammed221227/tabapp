import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tap_design/config/api_config.dart';
import 'package:tap_design/injection_container.dart';
import 'package:tap_design/screens/domain/entities/interest_model.dart';

class ListOfInterests extends StatefulWidget {
  final List<InterestModel> passions;
  final List<String> selectedPassions;
  final Function onTap;

  ListOfInterests(
      {@required this.passions,
      @required this.selectedPassions,
      @required this.onTap});

  @override
  _ListOfInterestsState createState() => _ListOfInterestsState();
}

class _ListOfInterestsState extends State<ListOfInterests> {
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(
      Duration(seconds: 1),
      () => _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 15000),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        width: double.infinity,
        height: 350,
        child: GridView(
            scrollDirection: Axis.horizontal,
            controller: _scrollController,
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 120,
                childAspectRatio: 1,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10),
            children: widget.passions.map(
              (passion) {
                int index = widget.passions.indexOf(passion);
                return Row(
                  children: [
                    Flexible(
                      child: AnimatedPassionItem(
                        onTap: () {
                          widget.onTap(passion.id);
                        },
                        isSelected:
                            widget.selectedPassions.contains(passion.id),
                        interestModel: passion,
                        index: index,
                      ),
                    ),
                  ],
                );
              },
            ).toList()),
      ),
    );
  }
}

class AnimatedPassionItem extends StatefulWidget {
  final int index;

  final bool isSelected;
  final InterestModel interestModel;
  final Function onTap;

  AnimatedPassionItem(
      {@required this.index,
      @required this.interestModel,
      @required this.isSelected,
      @required this.onTap});

  @override
  _AnimatedPassionItemState createState() => _AnimatedPassionItemState();
}

class _AnimatedPassionItemState extends State<AnimatedPassionItem> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onTap();
      },
      child: Container(
        width: widget.index % 2 != 0 ? 190 : 80,
        height: widget.index % 2 != 0 ? 190 : 80,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(widget.isSelected ? 1 : 0.4),
                    BlendMode.dstATop),
                image: NetworkImage(
                    "${sl<ApiConfig>().imageBaseUrl}${widget.interestModel.image}"),
                fit: BoxFit.cover)),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              widget.interestModel.name,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 11,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
