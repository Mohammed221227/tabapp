import 'package:flutter/material.dart';
import 'package:tap_design/config/colors.dart';
import 'package:tap_design/shared_widgets/btn.dart';
import 'package:tap_design/utils/hex_color.dart';

Widget userInstrucations() {
  return Column(
    children: [
      Text(
        "What are you into?",
        style: TextStyle(color: Colors.white, fontSize: 32),
      ),
      SizedBox(
        height: 10,
      ),
      Text(
        "Pick at least 5",
        style: TextStyle(color: Colors.white, fontSize: 13),
      )
    ],
  );
}

Widget verticalSeprator({double val = 10}) {
  return SizedBox(
    height: val,
  );
}


Widget horizentalSeprator({double val = 10}) {
  return SizedBox(
    width: val,
  );
}

Widget nextBtn({@required BuildContext context, @required Function onTap}) {
  return gradientBtn(
      onTap: onTap,
      startColor: lightBlue,
      endColor: darkBlue,
      title: "CONTINUE");
}

Widget progressIndicator(BuildContext context, {int totalSelected}) {
  return Container(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Flexible(
          child: Container(
            width: MediaQuery.of(context).size.width / 1.1,
            child: LinearProgressIndicator(
              value: totalSelected > 5 ? 1 : totalSelected / 5,
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
              minHeight: 1,
              backgroundColor: blueGray,
            ),
          ),
        ),
      ],
    ),
  );
}
