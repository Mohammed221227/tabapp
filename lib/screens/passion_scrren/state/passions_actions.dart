



import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:tap_design/config/enum.dart';
import 'package:tap_design/injection_container.dart';
import 'package:tap_design/screens/app_state/app_state.dart';
import 'package:tap_design/screens/domain/entities/interest_model.dart';
import 'package:tap_design/screens/domain/usecases/get_interest_use_case.dart';
import 'package:tap_design/screens/passion_scrren/state/passion_store.dart';

class AddPassionAction {
 final String passionId;

  AddPassionAction(this.passionId);
}

class DeletePassionAction {
  final String passionId;

  DeletePassionAction(this.passionId);
}


class ChangePassionStateAction {
  final RequestDataState requestDataState;

  ChangePassionStateAction(this.requestDataState);
}

class GetInterestsActions {
  final List<InterestModel> interests;

  GetInterestsActions(this.interests);
}

ThunkAction<AppState> getPassionsAction = (Store<AppState> store) async {
  store.dispatch(ChangePassionStateAction(RequestDataState.LOADING));
  List<InterestModel> interests = await sl<GetInterestUseCase>().call();
  store.dispatch(GetInterestsActions(interests));
  store.dispatch(ChangePassionStateAction(RequestDataState.LOADED));
};