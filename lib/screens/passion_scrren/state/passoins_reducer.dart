import 'package:tap_design/config/enum.dart';
import 'package:tap_design/screens/passion_scrren/state/passion_store.dart';
import 'package:tap_design/screens/passion_scrren/state/passions_actions.dart';

PassionSate passionStateReducer(PassionSate state, dynamic action) {
  if (action is AddPassionAction) {
    state.selectedPassoins.add(action.passionId);
    return PassionSate(
        selectedPassoins: state.selectedPassoins,
        passions: state.passions,
        passionState: state.passionState);
  } else if (action is DeletePassionAction) {
    state.selectedPassoins
        .removeWhere((element) => element == action.passionId);
    return PassionSate(
        selectedPassoins: state.selectedPassoins,
        passions: state.passions,
        passionState: state.passionState);
  } else if (action is GetInterestsActions) {
    return PassionSate(
        selectedPassoins: state.selectedPassoins, passions: action.interests);
  } else if (action is ChangePassionStateAction) {
    return PassionSate(
        selectedPassoins: state.selectedPassoins,
        passionState: action.requestDataState,
        passions: state.passions);
  }

  return state;
}

//
