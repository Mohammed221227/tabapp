import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';
import 'package:tap_design/config/enum.dart';
import 'package:tap_design/screens/domain/entities/interest_model.dart';

enum PassionActoin { ADD_PASSWION, DELETE_PASSION }

class PassionSate {
  List<String> selectedPassoins;
  List<InterestModel> passions;
  RequestDataState passionState;

  PassionSate(
      {this.selectedPassoins = const [],
      this.passions = const [],
      this.passionState = RequestDataState.IDLE});

  PassionSate.fromPassionState(PassionSate passionSate) {
    selectedPassoins = passionSate.selectedPassoins;
  }
}
