import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:get/get.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:tap_design/config/enum.dart';

import 'package:tap_design/res.dart';
import 'package:tap_design/screens/app_state/app_state.dart';
import 'package:tap_design/screens/domain/entities/interest_model.dart';
import 'package:tap_design/screens/domain/usecases/get_interest_use_case.dart';
import 'package:tap_design/screens/passion_scrren/state/passion_store.dart';
import 'package:tap_design/screens/passion_scrren/state/passions_actions.dart';
import 'package:tap_design/screens/passion_scrren/state/passoins_reducer.dart';
import 'package:tap_design/screens/passion_scrren/widgets/passions_list_widget.dart';
import 'package:tap_design/screens/passion_scrren/widgets/widgets.dart';
import 'package:tap_design/screens/profile_screen/profile_screen.dart';
import 'package:tap_design/utils/hex_color.dart';

import '../../data.dart';
import '../../injection_container.dart';

class ChoosePassionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage(Res.background), fit: BoxFit.cover),
      ),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            title: Text(
              "PASSIONS",
              style: TextStyle(color: Colors.white),
            ),
            centerTitle: true,
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              onPressed: () {},
            ),
          ),
          body: StoreConnector<AppState, PassionSate>(
              converter: (store) => store.state.passionSate,
              onInitialBuild: (PassionSate passionSate) {
                StoreProvider.of<AppState>(context).dispatch(getPassionsAction);
              },
              builder: (context, PassionSate passionSate) => passionSate
                          .passionState ==
                      RequestDataState.LOADED
                  ? ListView(
                      children: [
                        verticalSeprator(val: 45),
                        userInstrucations(),
                        verticalSeprator(val: 45),
                        ListOfInterests(
                            passions: passionSate.passions,
                            selectedPassions: passionSate.selectedPassoins,
                            onTap: (String id) {
                              _updatePassions(
                                  context: context,
                                  passionId: id,
                                  selectedPassions:
                                      passionSate.selectedPassoins);
                            }),
                        verticalSeprator(val: 25),
                        progressIndicator(context,
                            totalSelected: passionSate.selectedPassoins.length),
                        verticalSeprator(val: 25),
                        nextBtn(
                            context: context,
                            onTap: () {
                              if (passionSate.selectedPassoins.length >= 5) {
                                Get.to(ProfileScreen());
                              } else {
                                Get.snackbar(
                                    "message", "Pick at least 5 passions");
                              }
                            }),
                        verticalSeprator(val: 45),
                      ],
                    )
                  : Center(
                      child: CircularProgressIndicator(),
                    ))),
    );
  }

  _updatePassions(
      {@required BuildContext context,
      String passionId,
      List<String> selectedPassions}) {
    bool passionSelected = selectedPassions.contains(passionId);
    if (passionSelected) {
      StoreProvider.of<AppState>(context).dispatch(
        DeletePassionAction(passionId),
      );
    } else {
      StoreProvider.of<AppState>(context).dispatch(AddPassionAction(passionId));
    }
  }
}
