import 'package:tap_design/screens/passion_scrren/state/passoins_reducer.dart';
import 'package:tap_design/screens/profile_screen/state/profile_reducer.dart';

import 'app_state.dart';

AppState appStateReducer(AppState appState, action) {
  return AppState(
      passionSate: passionStateReducer(appState.passionSate, action),
      profileState: profileStateReducer(appState.profileState, action));
}
