import 'package:tap_design/screens/profile_screen/state/profile_actions.dart';
import 'package:tap_design/screens/profile_screen/state/profile_store.dart';

ProfileState profileStateReducer(ProfileState state, dynamic action) {
  if (action is AddSelectedPage) {
    state.selectedSlider = action.pageId;
    return ProfileState(selectedSlider: state.selectedSlider);
  } else if (action is SetUserProfileDataAction) {
    return ProfileState(
        selectedSlider: state.selectedSlider,
        userProfileModel: action.userProfileModel,
        profileState: state.profileState);
  } else if (action is ChangeProfileDataStateAction) {
    return ProfileState(
        selectedSlider: state.selectedSlider,
        profileState: action.requestDataState,
        userProfileModel: state.userProfileModel);
  }

  return state;
}
