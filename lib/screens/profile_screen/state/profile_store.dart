import 'package:tap_design/config/enum.dart';
import 'package:tap_design/screens/domain/entities/user_profile_model.dart';

class ProfileState {
  int selectedSlider;
  RequestDataState profileState;
  UserProfileModel userProfileModel;

  ProfileState(
      {this.selectedSlider = 0,
      this.profileState = RequestDataState.IDLE,
      this.userProfileModel});
}
