import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:tap_design/config/enum.dart';
import 'package:tap_design/screens/app_state/app_state.dart';
import 'package:tap_design/screens/domain/entities/user_profile_model.dart';
import 'package:tap_design/screens/domain/usecases/get_interest_use_case.dart';
import 'package:tap_design/screens/domain/usecases/get_user_profile_use_case.dart';

import '../../../injection_container.dart';

class AddSelectedPage {
  final int pageId;

  AddSelectedPage(this.pageId);
}

class ChangeProfileDataStateAction {
  final RequestDataState requestDataState;

  ChangeProfileDataStateAction(this.requestDataState);
}

class SetUserProfileDataAction {
  final UserProfileModel userProfileModel;

  SetUserProfileDataAction(this.userProfileModel);
}

ThunkAction<AppState> getUserProfileData = (Store<AppState> store) async {
  store.dispatch(ChangeProfileDataStateAction(RequestDataState.LOADING));
  UserProfileModel userProfileModel = await sl<GetUserProfileUseCase>().call();
  store.dispatch(SetUserProfileDataAction(userProfileModel));
  store.dispatch(ChangeProfileDataStateAction(RequestDataState.LOADED));
};
