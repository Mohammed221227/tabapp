import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:get/get.dart';
import 'package:tap_design/config/colors.dart';
import 'package:tap_design/screens/profile_screen/state/profile_actions.dart';
import 'package:tap_design/screens/profile_screen/state/profile_store.dart';

import 'package:tap_design/utils/hex_color.dart';

import '../../../res.dart';

double height = 400.0;

//profileCaroselSlider
Widget profileCaroselSlider(BuildContext context,
    {@required int selectedSliderIndex , @required String profileImage}) {
  return Stack(
    children: [
      CarouselSlider(
        options: CarouselOptions(
            height: height,
            viewportFraction: 1,
            onPageChanged: (index, readon) {
              StoreProvider.of<ProfileState>(context)
                  .dispatch(AddSelectedPage(index));
            }),
        items: [1, 2, 3, 4, 5].map((i) {
          return Builder(
            builder: (BuildContext context) {
              return Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(profileImage), fit: BoxFit.cover)));
            },
          );
        }).toList(),
      ),
      Positioned.fill(
        top: height / 1.4,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [1, 2, 3, 4, 5].map((index) {
            bool isSlected = index - 1 == selectedSliderIndex;

            return Padding(
              padding: const EdgeInsets.all(4.0),
              child: Container(
                width: isSlected ? 6 : 4,
                height: isSlected ? 6 : 4,
                margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: 2.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: isSlected
                      ? Colors.white
                      : Color(0xffd6d7d8).withOpacity(0.3),
                ),
              ),
            );
          }).toList(),
        ),
      ),
      Positioned(
        top: height / 2,
        left: Get.width / 1.2,
        child: Column(
          children: [
            iconBtn(iconData: Icons.favorite_border, onTap: () {}),
            iconBtn(iconData: Icons.thumb_down_alt_outlined, onTap: () {})
          ],
        ),
      ),
    ],
  );
}

Widget iconBtn({@required IconData iconData, @required Function onTap}) {
  return InkWell(
    onTap: onTap,
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: 50,
        height: 50,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [darkBlue.withOpacity(1), lightBlue.withOpacity(0.3)]),
            shape: BoxShape.circle),
        child: Icon(
          iconData,
          color: Colors.white,
        ),
      ),
    ),
  );
}
