import 'package:flutter/material.dart';
import 'package:tap_design/config/colors.dart';
import 'package:tap_design/utils/hex_color.dart';

Widget informationTable({@required String title, List<Widget> items}) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 8),
    child: Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: Text(
              title,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 17),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            decoration: BoxDecoration(
                color: cayanColor.withOpacity(0.4),
                borderRadius: BorderRadius.circular(10)),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: items,
              ),
            ),
          )
        ],
      ),
    ),
  );
}

Widget informationTableItem({@required String title, @required String value}) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          title,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          value,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
    ],
  );
}

Widget customDivider() {
  return Divider(
    color: Colors.white.withOpacity(0.4),
    height: 20,
  );
}
