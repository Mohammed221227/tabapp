import 'package:flutter/material.dart';
import 'package:tap_design/screens/data/model/profile_reponse_model.dart';
import 'package:tap_design/screens/domain/entities/user_profile_model.dart';

Widget personBasicInfo(PersonBasicInfo personBasicInfo) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 32,vertical: 8),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Text(
            "${personBasicInfo.name}, ${personBasicInfo.age}",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 17),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Text(
            personBasicInfo.bio,
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 17),
          ),
        ),
      ],
    ),
  );
}
