import 'package:flutter/material.dart';
import 'package:tap_design/res.dart';

Widget instaPosts(List<int> list) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 16),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Text(
            "100 Instagram Posts",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 17),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          height: 200,
          child: GridView(
              scrollDirection: Axis.horizontal,
              primary: false,
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 150,
                  childAspectRatio: 1,
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 20),
              children: list
                  .map(
                    (e) => InkWell(
                      onTap: () {},
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            image: DecorationImage(
                                image: AssetImage(Res.img_holder),
                                fit: BoxFit.cover)),
                      ),
                    ),
                  )
                  .toList()),
        ),
      ],
    ),
  );
}
