import 'package:bubble_chart/bubble_chart.dart';
import 'package:flutter/material.dart';
import 'package:tap_design/config/colors.dart';
import 'package:tap_design/screens/domain/entities/interest_model.dart';
import 'package:tap_design/utils/hex_color.dart';

Widget userPassions(List<InterestModel> passions) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 32),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Text(
            "Passions",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 17),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Container(height: 300, child: profilePassion(passions))
      ],
    ),
  );
}

Widget profilePassion(List<InterestModel> passions) {
  return BubbleChartLayout(
    root: BubbleNode.node(
        padding: 15,
        children: passions
            .map(
              (e) => BubbleNode.leaf(
                options: BubbleOptions(
                    color: primaryColor,
                    child: Container(
                      child: Center(
                        child: Text(
                          e.name,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: (9 + passions.indexOf(e)).toDouble()),
                        ),
                      ),
                    )),
                value: passions.indexOf(e) * 4159,
              ),
            )
            .toList()),
  );
}
