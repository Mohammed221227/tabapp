import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:tap_design/config/enum.dart';

import 'package:tap_design/res.dart';
import 'package:tap_design/screens/app_state/app_state.dart';
import 'package:tap_design/screens/profile_screen/state/profile_actions.dart';
import 'package:tap_design/screens/profile_screen/state/profile_reducer.dart';
import 'package:tap_design/screens/profile_screen/state/profile_store.dart';
import 'package:tap_design/screens/profile_screen/widget/profile_info_widgets.dart';
import 'package:tap_design/screens/profile_screen/widget/profile_insta_post_widgets.dart';
import 'package:tap_design/screens/profile_screen/widget/profile_passion_widget.dart';
import 'package:tap_design/screens/profile_screen/widget/profile_personal_info_widget.dart';
import 'package:tap_design/screens/profile_screen/widget/profile_slider.dart';
import 'package:tap_design/shared_widgets/btn.dart';
import 'package:tap_design/utils/hex_color.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(Res.background), fit: BoxFit.cover),
        ),
        child: StoreConnector<AppState, ProfileState>(
          converter: (store) => store.state.profileState,
          onInitialBuild: (ProfileState profileState) {
            StoreProvider.of<AppState>(context).dispatch(getUserProfileData);
          },
          builder: (context, ProfileState profileState) => Container(
            child: profileState.profileState == RequestDataState.LOADED
                ? Scaffold(
                    backgroundColor: Colors.transparent,
                    body: NestedScrollView(
                        headerSliverBuilder:
                            (BuildContext context, bool innerBoxIsScrolled) {
                          return <Widget>[
                            SliverAppBar(
                              expandedHeight: 300,
                              floating: true,
                              snap: false,
                              backgroundColor: Colors.transparent,
                              pinned: true,
                              flexibleSpace: FlexibleSpaceBar(
                                  background: profileCaroselSlider(context,
                                      selectedSliderIndex:
                                          profileState.selectedSlider,
                                      profileImage: profileState
                                          .userProfileModel
                                          .basicInfo
                                          .profilePicture)),
                            ),
                          ];
                        },
                        //dependices
                        body: ListView(
                          children: [
                            personBasicInfo(
                                profileState.userProfileModel.basicInfo),
                            informationTable(title: "Basic Info", items: [
                              informationTableItem(
                                  title: "Name",
                                  value: profileState
                                      .userProfileModel.basicInfo.name),
                              customDivider(),
                              informationTableItem(
                                  title: "Gender",
                                  value: profileState
                                      .userProfileModel.basicInfo.gender),
                              customDivider(),
                              informationTableItem(
                                  title: "Age",
                                  value: profileState
                                      .userProfileModel.basicInfo.age
                                      .toString()),
                              customDivider(),
                              informationTableItem(
                                  title: "Location", value: "Location"),
                            ]),
                            informationTable(
                                title: "Personal Info",
                                items: profileState
                                    .userProfileModel.personalInfo
                                    .map((personalInfo) => Column(
                                          children: [
                                            informationTableItem(
                                                title: personalInfo.question,
                                                value: personalInfo.answer),
                                            customDivider(),
                                          ],
                                        ))
                                    .toList()),
                            instaPosts([1, 2, 3, 4, 4, 4, 4, 4]),
                            userPassions(
                                profileState.userProfileModel.passions),
                            SizedBox(
                              height: 20,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 32),
                              child: FittedBox(
                                child: Column(
                                  children: [
                                    normalBtn(
                                        title: "REPORT",
                                        color: HexColor("#081C71"),
                                        onTap: () {}),
                                    normalBtn(
                                        title: "UNPAIR",
                                        color: HexColor("#2699FB")
                                            .withOpacity(0.4),
                                        onTap: () {}),
                                    normalBtn(
                                        title: "BLOCK",
                                        color: HexColor("#DEDEDE")
                                            .withOpacity(0.4),
                                        onTap: () {})
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 45,
                            ),
                          ],
                        )),
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          ),
        ));
  }
}
